package com.example.demo.controller;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.model.Employee;
import com.example.demo.model.RecordNotFoundException;
import com.example.demo.service.EmployeService;

@RestController
public class EmployeeController {
	
	
	Employee empRecord=null;

	@Autowired
	private EmployeService employeeService;

	@PostMapping("employees/save")
	public ResponseEntity<Employee> save(@RequestBody Employee employee) {

		employee.setDateOfBirth(new Date());

		return ResponseEntity.status(200).body(employeeService.save(employee));
	}

	@GetMapping("employees/listallemployees")
	public ResponseEntity<List<Employee>> getAllEmployee() {
		return ResponseEntity.status(200).body(employeeService.listOfEmployee());
	}

	@GetMapping("employees/listallemployees/{id}")
	public ResponseEntity<Employee> findById(@PathVariable("id") String id) {
		
		empRecord=employeeService.findId(id);
		if(empRecord==null)
			throw new RecordNotFoundException("record not found");
		
          return ResponseEntity.status(200).body(employeeService.findId(id));
	}

}
