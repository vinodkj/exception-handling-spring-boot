package com.example.demo.service;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Predicate;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo.model.Employee;

@Service
public class EmployeService {

	@Autowired
	Employee employee;

	List<Employee> empList = new ArrayList<>();

	public Employee save(Employee emp) {
		empList.add(emp);
		return emp;
	}

	public List<Employee> listOfEmployee() {
		return empList;
	}

	public Employee findId(String id) {

		for (Employee emp : empList) {
			if (emp.getId().equalsIgnoreCase(id)) {
				return emp;
			}
		}

		return null;

	}

}
