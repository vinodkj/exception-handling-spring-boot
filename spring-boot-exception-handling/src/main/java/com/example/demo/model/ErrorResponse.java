package com.example.demo.model;

import java.util.List;

public class ErrorResponse {

	private String message;
	private List<String> list;

	public ErrorResponse(String message, List<String> list) {
		super();
		this.message = message;
		this.list = list;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public List<String> getList() {
		return list;
	}

	public void setList(List<String> list) {
		this.list = list;
	}

}
